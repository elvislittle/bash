#!/bin/bash

printf '\33c\e[3J'

#!/bin/bash

# Check if jq is installed

if ! command -v jq &> /dev/null; then
    echo -e "\njq is not installed. Installing...\n"
    if ! command -v brew &> /dev/null; then
        echo "Homebrew is not installed. Installing Homebrew..."
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
        if [ $? -ne 0 ]; then
            echo "Failed to install Homebrew."
            exit 1
        fi
    fi

    brew install jq
    if [ $? -eq 0 ]; then
        echo -e "\njq has been successfully installed\n"
    else
        echo -e "\nFailed to install jq\n"
        exit 1
    fi
fi

printf '\33c\e[3J'

cd "$(dirname "$0")"

# Check if the JSON file exists

if [ ! -f "./tasks.json" ]; then
    echo -e "\nError: JSON file 'tasks.json' not found\n"
    read -p "Press Enter to exit"
    exit 1
fi

# Retrieve the current date in the format YYYY-MM-DD

current_date=$(date +%F)

# Flag to track if overdue tasks were found

overdue_found=0

# Iterate through tasks in the JSON file

while IFS= read -r task; do

    # Extract task details

    task_id=$(jq -r '.id' <<< "$task")
    task_title=$(jq -r '.title' <<< "$task")
    task_description=$(jq -r '.description' <<< "$task")
    task_status=$(jq -r '.status' <<< "$task")
    task_deadline=$(jq -r '.deadline' <<< "$task")

    # Check if task deadline has passed

    if [[ "$task_deadline" < "$current_date" ]]; then
        overdue_found=1

        # Output information about overdue tasks

        echo -e "Overdue Task:\n"
        echo "  ID: $task_id"
        echo "  Title: $task_title"
        echo "  Description: $task_description"
        echo "  Status: $task_status"
        echo -e "  Deadline: $task_deadline\n"
    fi
done < <(jq -c '.tasks[]' tasks.json)

# If overdue tasks were found, display the appropriate message

if [ $overdue_found -eq 1 ]; then
    echo -e "Overdue tasks were found\n"
else
    echo -e "\nNo overdue tasks found\n"
fi

read -p "Press Enter to exit"
